package icurious.timemachine;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

import icurious.timachine.TiMachine;

public class MainActivity extends AppCompatActivity {

    TextView output;
    EditText input;

    private SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private InputMethodManager inputManager;
    private int mInterval = 1000; // 5 seconds by default, can be changed later
    private Handler mHandler;
    private String bod;
    private boolean isRunning;
    private TiMachine tiMachine;


    public void onClickButton(View view) {
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        bod = input.getText().toString();
        if (!isRunning) {
            startRepeatingTask();
            isRunning = true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        output = (TextView) findViewById(R.id.output);
        input = (EditText) findViewById(R.id.input);
        tiMachine = new TiMachine();
        inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mHandler = new Handler();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
//                getDuration(); //this function can change value of mInterval.
                output.setText(tiMachine.getDuration(bod));
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };


    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }
}
