package icurious.timachine;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Towhid on 7/4/17.
 */

public class TiMachine {

    private SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    public String getDuration(String from) {
        long millis;
        try {
            Date dob = this.from.parse(from);
            millis = new Date().getTime() - dob.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return "invalid input";
        }

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(millis);
        c.set(Calendar.YEAR, c.get(Calendar.YEAR) - 1970);
        c.set(Calendar.MONTH, c.get(Calendar.MONTH) - 1);

        return this.from.format(c.getTime())
                + "\n" + c.get(Calendar.YEAR)  + "year " +
                c.get(Calendar.MONTH) + "month " +
                c.get(Calendar.DAY_OF_MONTH) + "day\n" +
                c.get(Calendar.HOUR) + "hour " +
                c.get(Calendar.MINUTE) + "min " +
                c.get(Calendar.SECOND) + "sec"
                ;
    }

}
